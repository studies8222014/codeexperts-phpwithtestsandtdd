<?php

namespace Code;

class Log
{
    public function log(string $message): string
    {
        return 'Logando dados no sistema: ' . $message;
    }
}
