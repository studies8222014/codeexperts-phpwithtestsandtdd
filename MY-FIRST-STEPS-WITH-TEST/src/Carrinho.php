<?php

namespace Code;

class Carrinho
{
    private $produtos = [];

    public function addProduto($produto, Log $log = null)
    {
        $this->produtos[] = $produto;
        if (!is_null($log))
            $log->log('Qualquer coisa');
    }

    public function getProdutos()
    {
        return $this->produtos;
    }

    public function totalProdutos()
    {
        return count($this->produtos);
    }

    public function getTotalCompra()
    {
        return array_reduce($this->produtos, function ($acumulado, $atual) {
            $acumulado += $atual->getPrice();
            return $acumulado;
        }, 0);
    }
}
