<?php

namespace Code;

use PHPUnit\Framework\TestCase;
use Code\Produto;

class CarrinhoTest extends TestCase
{
    // manipular varios produtos
    // visualizar produtos
    // total de produtos / total compra
    private $carrinho;
    private $produto;

    public function setUp(): void
    {
        $this->carrinho = new Carrinho;
    }

    public function tearDown(): void
    {
        unset($this->carrinho);
    }

    // public function testSeClasseCarrinhoExiste()
    // {
    //     $classe = class_exists("\\Code\\Carrinho");

    //     $this->assertTrue($classe);
    // }

    protected function assertPreConditions(): void
    {
        $classe = class_exists(Carrinho::class);

        $this->assertTrue($classe);
    }

    public function testAdicaoProdutosNoCarrinho()
    {
        $produto1 = new Produto;
        $produto1->setName('Produto 1');
        $produto1->setPrice(19.90);
        $produto1->setSlug('produto-1');

        $produto2 = new Produto;
        $produto2->setName('Produto 2');
        $produto2->setPrice(7.55);
        $produto2->setSlug('produto-2');

        $produto3 = new Produto;
        $produto3->setName('Produto 3');
        $produto3->setPrice(109.01);
        $produto3->setSlug('produto-3');

        $carrinho = $this->carrinho;
        $carrinho->addProduto($produto1);
        $carrinho->addProduto($produto2);
        $carrinho->addProduto($produto3);

        $this->assertIsArray($carrinho->getProdutos());
        $this->assertInstanceOf(Produto::class, $carrinho->getProdutos()[0]);
        $this->assertInstanceOf(Produto::class, $carrinho->getProdutos()[1]);
        $this->assertInstanceOf(Produto::class, $carrinho->getProdutos()[2]);
    }

    public function testSeValoresDeProdutosNoCarrinhoEstaoCorretosConformePassado()
    {
        // $produto1 = new Produto;
        // $produto1->setName('Produto 1');
        // $produto1->setPrice(19.90);
        // $produto1->setSlug('produto-1');

        $produtoStub = $this->getStubProduto();

        $carrinho = $this->carrinho;
        $carrinho->addProduto($produtoStub);

        $this->assertEquals('Produto 1', $carrinho->getProdutos()[0]->getName());
        $this->assertEquals(19.90, $carrinho->getProdutos()[0]->getPrice());
        $this->assertEquals('produto-1', $carrinho->getProdutos()[0]->getSlug());
    }

    public function testSeTotalDeProdutosEValorDaCompraEstaoCorretos()
    {
        $produto1 = new Produto;
        $produto1->setName('Produto 1');
        $produto1->setPrice(19.90);
        $produto1->setSlug('produto-1');

        $produto2 = new Produto;
        $produto2->setName('Produto 2');
        $produto2->setPrice(7.55);
        $produto2->setSlug('produto-2');

        $produto3 = new Produto;
        $produto3->setName('Produto 3');
        $produto3->setPrice(109.7);
        $produto3->setSlug('produto-3');

        $carrinho = $this->carrinho;
        $carrinho->addProduto($produto1);
        $carrinho->addProduto($produto2);
        $carrinho->addProduto($produto3);

        $this->assertEquals(3, $carrinho->totalProdutos());
        $this->assertEquals(137.15, $carrinho->getTotalCompra());
    }

    public function testImcompleto()
    {
        $this->assertTrue(true);
        $this->markTestIncomplete('Teste não está completo!');
    }

    /**
     * @requires PHP == 5.3
     */
    public function testSeFeatureEspecificaParaVersao53PhpTrabalhaDeFormaEsperada()
    {
        // if (PHP_VERSION > 7) {
        //     $this->markTestSkipped('Este teste só roda para versão abaixo do PHP 7.');
        // }
        $this->assertTrue(true);
    }

    public function testSeSetSlugLancaExceptionQuandoNaoInformado()
    {
        $this->expectException('\InvalidArgumentException');
        $this->expectExceptionMessage('Parâmetro inválido, informe um slug');

        $produto = new Produto;
        $produto->setSlug('');
    }

    public function testSeLogESalvoQuandoInformadoParaAAdicaoDeProduto()
    {
        $carrinho = $this->carrinho;

        $logMock = $this->getMockBuilder(Log::class)
            ->onlyMethods(['log'])
            ->getMock();

        $logMock->expects($this->once())
            ->method('log')
            ->with($this->equalTo('Qualquer coisa'));

        $carrinho->addProduto($this->getStubProduto(), $logMock);
    }

    private function getStubProduto()
    {
        $produtoStub = $this->createMock(Produto::class);
        $produtoStub->method('getName')->willReturn('Produto 1');
        $produtoStub->method('getPrice')->willReturn(19.90);
        $produtoStub->method('getSlug')->willReturn('produto-1');
        return $produtoStub;
    }
}
