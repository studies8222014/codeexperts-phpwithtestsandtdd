<?php

namespace CodeTests\QueryBuilder\Query;

use Code\QueryBuilder\Query\Insert;
use PHPUnit\Framework\TestCase;

class InsertTest extends TestCase
{
    private $insert;

    protected function assertPreConditions(): void
    {
        $this->assertTrue(class_exists(Insert::class));
    }

    protected function setUp(): void
    {
        $this->insert = new Insert('products', ['name', 'price']);
    }

    public function testIfInsertionQueryHasGeneratedWithSuccess()
    {
        $sql1 = "insert into products(name, price) values(:name, :price)";
        // $sql2 = "insert into products values(:name, :price)";

        $this->assertEquals($sql1, $this->insert->getSql());

        // $this->insert = new Insert('products');

        return $this;
    }
}
