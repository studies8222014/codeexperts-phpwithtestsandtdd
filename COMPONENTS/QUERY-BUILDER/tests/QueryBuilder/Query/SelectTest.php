<?php

namespace CodeTests\QueryBuilder\Query;

use PHPUnit\Framework\TestCase;

use Code\QueryBuilder\Query\Select;

class SelectTest extends TestCase
{
    protected $select;

    protected function assertPreConditions(): void
    {
        $this->assertTrue(class_exists(Select::class));
    }

    public function setUp(): void
    {
        $this->select = new Select('products');
    }

    public function testIfQueryBaseIsGenerateWithSuccess()
    {
        $query = $this->select->getSql();

        $this->assertEquals('select * from products', $query);
    }

    public function testIfQueryIsGenerateWithWhereConditions()
    {
        $query = $this->select->where('name', '=', ':name');

        $this->assertEquals('select * from products where name = :name', $query->getSql());
    }

    public function testIfQueryAllowUsAddMoreConditionsInOurQueryWithWhere()
    {
        $query = $this->select->where('name', '=', ':name')
            ->where('price', '>=', ':price');

        $this->assertEquals('select * from products where name = :name and price >= :price', $query->getSql());
    }

    public function testIfQueryIsGenerateWithOrderBy()
    {
        $query = $this->select->orderBy('name', 'desc');

        $this->assertEquals('select * from products order by name desc', $query->getSql());
    }

    public function testIfQueryIsGenerateWithLimit()
    {
        $query = $this->select->limit(0, 15);

        $this->assertEquals('select * from products limit 0, 15', $query->getSql());
    }

    public function testIfQueryIsGenerateWithJoinsConditions()
    {
        $query = $this->select->join('inner join', 'colors', 'colors.product_id', '=', 'products.id');
        $this->assertEquals(
            'select * from products inner join colors on colors.product_id = products.id',
            $query->getSql()
        );
    }

    public function testIfQueryWithSelectedFieldsIsGeneratedWithSuccess()
    {
        $query = $this->select->select('name', 'price');
        $this->assertEquals('select name, price from products', $query->getSql());
    }

    public function testIfSelectQueryIsGeneratedWithMoreJoinsClausele()
    {
        $sql = "select name, price, created_at from products inner join colors on colors.product_id = products.id and colors.teste_id = products.teste_id left join categories on categories.id = products.category_id where id = :id";

        $query = $this->select->where('id', '=', ':id')
            ->join('inner join', 'colors', 'colors.product_id', '=', 'products.id')
            ->join('inner join', 'colors', 'colors.teste_id', '=', 'products.teste_id', 'and')
            ->join('left join', 'categories', 'categories.id', '=', 'products.category_id')
            ->select('name', 'price', 'created_at');

        $this->assertEquals($sql, $query->getSql());
    }
}
