<?php

namespace Code\QueryBuilder\Query;

class Update implements QueryInterface
{
    private $sql;

    public function __construct(string $table, array $fields = [], array $conditions = [], $logicOperator = 'and')
    {
        $this->sql = 'update ' . $table . ' set ';
        $set = '';
        foreach ($fields as $key => $value) {
            $set .= $set ? ', ' . $value . ' = :' . $value : $value . ' = :' . $value;
        }

        $where = '';
        foreach ($conditions as $key => $value) {
            $where .= $where ? ' ' . $logicOperator . ' ' . $key . ' = ' . $value : ' where ' . $key . ' = ' . $value;
        }
        $this->sql .= $set . $where;
        return $this;
    }

    public function getSql()
    {
        return $this->sql;
    }
}
