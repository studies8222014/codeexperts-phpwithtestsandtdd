<?php

namespace Code\QueryBuilder\Query;

class Insert implements QueryInterface
{
    private $sql;

    public function __construct(string $table, array $fields = [])
    {
        $this->sql = 'insert into ' . $table;

        if (count($fields) > 0)
            $this->sql .= '(' . implode(', ', $fields) . ')';

        $this->sql .= ' values(:' . implode(', :', $fields) . ')';
    }

    public function getSql()
    {
        return $this->sql;
    }
}
