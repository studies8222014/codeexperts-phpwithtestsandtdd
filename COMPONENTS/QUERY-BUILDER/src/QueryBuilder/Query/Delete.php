<?php

namespace Code\QueryBuilder\Query;

class Delete implements QueryInterface
{
    private $sql;

    public function __construct($table, $conditions, $logicOperator = 'and')
    {
        $this->sql = 'delete from ' . $table;

        $where = '';
        foreach ($conditions as $key => $value) {
            $where .= $where ? ' ' . $logicOperator . ' ' . $key . ' = ' . $value : ' where ' . $key . ' = ' . $value;
        }
        $this->sql .= $where;
        return $this;
    }

    public function getSql()
    {
        return $this->sql;
    }
}
