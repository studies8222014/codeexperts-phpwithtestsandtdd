<?php

namespace Code\QueryBuilder;

use Code\QueryBuilder\Query\QueryInterface;
use PDO;

class Executor
{
    private $connection;
    /**
     * var QueryInterface
     */
    private $query;
    private $result;
    /**
     * @var array
     */
    private $params = [];

    public function __construct(PDO $connection, QueryInterface $query = null)
    {
        $this->connection = $connection;
        $this->query = $query;
    }

    public function setQuery(QueryInterface $query)
    {
        $this->query = $query;
    }

    public function setParam($bind, $value)
    {
        $this->params[] = ['bind' => $bind, 'value' => $value];
        return $this;
    }

    public function execute()
    {
        $process = $this->connection->prepare($this->query->getSql());

        if (count($this->params) > 0) {
            foreach ($this->params as $key => $param) {
                $type = gettype($param['value']) == 'integer' ? PDO::PARAM_INT : PDO::PARAM_STR;
                $process->bindValue($param['bind'], $param['value'], $type);
            }
        }

        $returnProcess = $process->execute();
        $this->result = $process;
        return $returnProcess;
    }

    public function getResult()
    {
        if (!$this->result) return null;
        return $this->result->fetchAll(PDO::FETCH_ASSOC);
    }
}
